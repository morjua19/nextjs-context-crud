'use client'
import React, { useState } from 'react';
import { useTasks } from '@/context/TasksContext';

export default function NewPage() {
  const [task, setTask] = useState();
  const {createTask} = useTasks()
  const handleChance = (e) => {
    // @ts-ignore
    setTask({...task, [e.target.name]: e.target.value} )
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(task)
    // @ts-ignore
    createTask(task.title, task.description)
  }

  return (
    <form onSubmit={handleSubmit}>
      <input type="text" placeholder='Write a title' name='title' onChange={handleChance}/>
      <textarea placeholder='Write a description' name='description' onChange={handleChance}/>
      <button>Save</button>
    </form>
  )
}
