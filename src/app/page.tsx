'use client'
import React from 'react';
import { useTasks } from '@/context/TasksContext';
import { Task } from '@/models/task.interface';
import { TaskCard } from '@/components/TaskCard';

export default function Page() {
  // @ts-ignore
  const {tasks} = useTasks();

  return (
    <div>
      {tasks.map((task: Task, i: number) => (
       <TaskCard task={task} key={i}></TaskCard>
      ))}
    </div>
  )
}
