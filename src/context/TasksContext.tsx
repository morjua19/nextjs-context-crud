'use client'
import { Context, createContext, useContext, useState } from 'react';
import { Task } from '@/models/task.interface';


// @ts-ignore
export const TaskContext: Context<unknown> = createContext()
export const useTasks = () => {
  const context = useContext(TaskContext)
  if (!context) throw new Error('useTasks must used within a provider')
  return context;
}
export const TaskProvider = ({ children }: any) => {
  const [tasks, setTasks] = useState([
    {
      id: 1,
      title: 'my first task',
      description: 'some task'
    },
    {
      id: 2,
      title: 'my second task',
      description: 'some second task'
    },
    {
      id: 3,
      title: 'my third task',
      description: 'some third task'
    }
  ])
  const createTask = (title: string, description: string) =>
    setTasks([...tasks, { id: 10, title, description }])


  return <TaskContext.Provider value={{
    tasks,
    createTask
  }}>
    {children}
  </TaskContext.Provider>
}
